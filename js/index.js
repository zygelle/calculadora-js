var calc = {
    buttons: document.getElementById('container'),
    display: document.getElementById('display'),
  }
  
  //Variavies que usaremos nessa calculadora
  var number1 = '';
  var number2 = '';
  var resultado = '';
  var operation = false;
  
  // Evento de clique no container
  calc.buttons.addEventListener('click', function(events){  
    switch(events.target.className) {
      case 'num':
        if(!operation) {
          number1 += events.target.innerText;   
          displayShow(number1);     
          break;
        } 
        number2 += events.target.innerText;
        displayShow(number2);
        calculadora(parseInt(number1), parseInt(number2), operation); // Reparem que passo para o tipo number
        posOperation(calc.display.innerText);
        break;    
      case 'op': // Ao clicar em um sinal de operacao, variavel recebe o sinal e exibimos na tela
        operation = events.target.innerText;
        displayShow(operation);
    } 
  });
  
  // Diversas funcoes
  function calculadora(a, b, c){
    switch(c) {
      case '+':
        resultado = (a+b);
        break
      case '-':
        resultado = (a-b);  
        //displayShow(a - b);
        break
      case '*':
        resultado = (a*b);
        //displayShow(a * b);
        break
      case '/':
        resultado = (a/b);
        //displayShow(a / b);
        break
      default:
        displayShow("Error")
    }    
  }

  function equals() {
    displayShow(resultado);
  }
  
  function reset() {
    number1 = number2 = '';  
    operation = false;
    displayShow(0);
  }

  //pega o primeiro numero mas não pega o decimal
  function decimal() {
      displayShow(number1+'.');
  }
  
  function posOperation(result){
    number1 = result;
    number2 = '';  
    operation = true;
  }
  
  function displayShow(exibirValor){
    if(exibirValor.length > 9 || calc.display.textContent.length > 9){
      calc.display.textContent = "overflow";
      return;
    }
    calc.display.textContent = exibirValor;
  }
  